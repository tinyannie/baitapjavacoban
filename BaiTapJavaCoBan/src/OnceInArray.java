import java.util.*;

public class OnceInArray {
	public static void main(String[] args) {
		System.out.println("Nhap so phan tu cua mang: ");
		Scanner sc=new Scanner(System.in);
		int n;
		n=sc.nextInt();
		int[] intArr=new int[n];
		System.out.println("Nhap cac phan tu cua mang: ");
		for(int i=0;i<n;i++){
			intArr[i]=sc.nextInt();
		}
		Map<Integer,Integer> intMap=new HashMap<Integer,Integer>();
		for(int i=0;i<n;i++){
			addRow(intArr[i],intMap);
		}
		System.out.print("Cac so xuat hien mot lan trong mang: ");
		numberFrequency(1,intMap);
	}
	private static void addRow(int key,Map<Integer,Integer> map){
		int count=1;
		if(map.containsKey(key)){
			count=map.get(key)+1;
		}
		map.put(key, count);
	}
	private static void numberFrequency(int frequency,Map<Integer,Integer> map){
		for(Integer key: map.keySet()){
			if(map.get(key)==frequency){
				System.out.print(key+" ");
			}
		}
	}
}
