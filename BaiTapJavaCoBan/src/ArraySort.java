import java.util.*;
public class ArraySort {
public static void main(String[] args) {
	System.out.println("Nhap so phan tu cua mang: ");
	Scanner sc=new Scanner(System.in);
	int n;
	n=sc.nextInt();
	int[] intArr=new int[n];
	System.out.println("Nhap cac phan tu cua mang: ");
	for(int i=0;i<n;i++){
		intArr[i]=sc.nextInt();
	}
	System.out.println("Descending Sort:");
	System.out.println(Arrays.toString(arrSort(intArr,false)));
	System.out.println("Ascending Sort:");
	int[] insertArr=new int[n];
	insertArr =arrSort(intArr,true);
	System.out.println(Arrays.toString(insertArr));
	System.out.print("x= ");
	int x;
	x=sc.nextInt();
	System.out.println("Sorted Insert:");
	System.out.println(Arrays.toString(sortedInsert(x,insertArr)));
}
public static int[] arrSort(int[] array,boolean bool){    
    int  position=0 ;  
    for(int i =0;i<array.length;i++){  
        int j=i+1;  
        position=i;  
        int temp=array[i];  
        for(;j<array.length;j++){  
            if(array[j]<temp==bool){  
                temp=array[j];  
                position=j;  
            	}  
        	}  
        array[position]=array[i];  
        array[i]=temp;  
    	}
    return array;
	}
public static int[] sortedInsert(int x,int[] sortedArr){
	int pos=0;
	int[] tmpArr=new int[sortedArr.length+1];
	for(int i=0;i<sortedArr.length;i++){
		if(x<=sortedArr[i]) {
			pos=i;
			break;
		}
		pos=sortedArr.length;
	}
	for(int i=sortedArr.length-1;i>=pos;i--){
		tmpArr[i+1]=sortedArr[i];
	}
	for(int i=0;i<pos;i++){
		tmpArr[i]=sortedArr[i];		
	}
	tmpArr[pos]=x;
	return tmpArr;
}
}