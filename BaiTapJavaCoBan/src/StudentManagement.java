import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Vector;

public class StudentManagement {
	ArrayList<Student> list=new ArrayList<Student>();
	Scanner sc=new Scanner(System.in);
	public StudentManagement() {
		Student st1=new Student("001","Le A",18,"Dna",2);
		Student st2=new Student("002","Le B",18,"Dna",4);
		Student st3=new Student("003","Le C",18,"Dna",3);
		list.add(st1);
		list.add(st2);
		list.add(st3);
		while(true) {
			System.out.println("/****************************************/");
			System.out.println(" 1. Add student. ");
			System.out.println(" 2. Edit student by id. ");
			System.out.println(" 3. Delete student by id. ");
			System.out.println(" 4. Sort student by gpa. ");
			System.out.println(" 5. Sort student by name. ");
			System.out.println(" 6. Show student. ");
			System.out.println(" 0. Exit ");
			System.out.println("/****************************************/");
			int choice;
			try{
				choice=sc.nextInt();
			}catch(Exception e){
				System.out.println("Invalid choice!");
				choice=0;
			}
			switch(choice) {
			case 1:this.add();break;
			case 2:this.edit();break;
			case 3:this.delete();break;
			case 4:this.sortGpa();break;
			case 5:this.sortName();break;
			case 6:this.show();break;
			case 0:return;
			}
		}
	}
	public void add() {
		String id;
		String name;
		int age;
		String address;
		double gpa;
		System.out.println("Add student.");
		System.out.print(" Id: ");
		sc.nextLine();
		id=sc.nextLine();
		System.out.print(" Name: ");
		name=sc.nextLine();
		System.out.print(" Age: ");
		age=sc.nextInt();
		System.out.print(" Adress: ");
		sc.nextLine();
		address=sc.nextLine();
		System.out.print(" Gpa: ");
		gpa=sc.nextDouble();
		Student st=new Student(id,name,age,address,gpa);
		list.add(st);
		System.out.println("/****************************************/");
	}
	public void show() {
		System.out.println("Show student.");
		int i=1;
		System.out.println("STT\t\tID\t\tNAME\t\t\t\tAGE\t\tADDRESS\t\t\tGPA");
		Iterator<Student> it=list.iterator();
		while(it.hasNext()) {
			Student sts=(Student) it.next();
			System.out.println(i+"\t\t"+sts.getId()+"\t\t"+sts.getName()+"\t\t\t\t"+sts.getAge()+"\t\t"+sts.getAddress()+"\t\t\t"+sts.getGpa());
			i++;
		}
		System.out.println("/****************************************/");
	}
	public void sortGpa() {
		Student [] st=new Student[list.size()];
		Iterator<Student> it=list.iterator();
		int i=0;
		while(it.hasNext()) {
			st[i]=(Student)it.next();
			i++;
		}
		//sap xep mang
		Arrays.sort(st);
		System.out.println("Sort student by gpa:Ascending sort");
		System.out.println("STT\t\tID\t\tNAME\t\t\t\tAGE\t\tADDRESS\t\t\tGPA");
		for(i=0;i<st.length;i++) {
			System.out.println(i+1+"\t\t"+st[i].getId()+"\t\t"+st[i].getName()+"\t\t\t\t"+st[i].getAge()+"\t\t"+st[i].getAddress()+"\t\t\t"+st[i].getGpa());
		}
		System.out.println("Sort student by gpa:Ascending sort");
		for(i=st.length-1;i>=0;i--) {
			System.out.println(st.length-i+"\t\t"+st[i].getId()+"\t\t"+st[i].getName()+"\t\t\t\t"+st[i].getAge()+"\t\t"+st[i].getAddress()+"\t\t\t"+st[i].getGpa());
		}
		System.out.println("/****************************************/");
	}
	public void sortName(){
		Student [] stArr=new Student[list.size()];
		Iterator<Student> it=list.iterator();
		int i=0;
		while(it.hasNext()){
			stArr[i]=(Student)it.next();
			i++;
		}
		System.out.println("Sort student by name.");
		System.out.print("Ascending sort=1, Descending sort=0 :");
		int sortChoice;
		sortChoice=sc.nextInt();
		if (sortChoice == 1) {
			System.out.println("Ascending sort ");
			showArr(sort(stArr,true));
			
		} else {
			System.out.println("Descending sort");
			showArr(sort(stArr,false));
		}
		System.out.println("/***************************************/");
	}
	public static Student [] sort(Student[] stArr,Boolean bool){
		int pos=0;
		for (int i=0;i<stArr.length;i++) {
			int j=i+1;
			pos=i;
			Student tmp=stArr[i];
			for (;j<stArr.length;j++) {
				if ((stArr[j].getName().compareTo(tmp.getName()) < 0)==bool) {
					tmp=stArr[j];
					pos=j;
				}
			}
			stArr[pos]=stArr[i];
			stArr[i]=tmp;
		}
		return stArr;
	}
	public static void showArr(Student [] stArr){
		System.out.println("STT\t\tID\t\tNAME\t\t\t\tAGE\t\tADDRESS\t\t\tGPA");
		for(int i=0;i<stArr.length;i++) {
			System.out.println(i+1+"\t\t"+stArr[i].getId()+"\t\t"+stArr[i].getName()+"\t\t\t\t"+stArr[i].getAge()+"\t\t"+stArr[i].getAddress()+"\t\t\t"+stArr[i].getGpa());
		}
	}
	public void edit() {
		System.out.println("Edit student by id.");
		System.out.print("Current id: ");
		sc.nextLine();
		String oldId=sc.nextLine();
		System.out.println("New id: ");
		String newId=sc.nextLine();
		System.out.print(" Name: ");
		String newName=sc.nextLine();
		System.out.print(" Age: ");
		int newAge=sc.nextInt();
		System.out.print(" Adress: ");
		sc.nextLine();
		String newAddress=sc.nextLine();
		System.out.print(" Gpa: ");
		double newGpa=sc.nextDouble();
		Iterator<Student> it=list.iterator();
		while(it.hasNext()) {
			Student st=(Student)it.next();
			if(st.getId().equals(oldId)) {
				st.setId(newId);
				st.setName(newName);
				st.setAge(newAge);
				st.setAddress(newAddress);
				st.setGpa(newGpa);
			}
		}
		System.out.println("/****************************************/");
	}
	public void delete(){
		System.out.println("Delete student by id.");
		System.out.print("Current id: ");
		sc.nextLine();
		String delId=sc.nextLine();
		Iterator<Student> it=list.iterator();
		while(it.hasNext()){
			Student st=(Student) it.next();
			if(st.getId().equals(delId)){
				System.out.println("1");
				it.remove();
			}
		}
	}
	public static void main(String[] args) {
		new StudentManagement();
	}
}
