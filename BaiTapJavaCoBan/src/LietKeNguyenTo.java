import java.util.Scanner;


public class LietKeNguyenTo {
public static void main(String[] args) {
	byte n;
	Scanner sc=new Scanner(System.in);
	System.out.print("n= ");
	n=sc.nextByte();
	if(isPrime(n)) System.out.println(n +" la so nguyen to");
	else System.out.println(n +" khong phai la so nguyen to");
}
public static boolean isPrime(byte n){
	if(n<=1) return false;
	else if(n==2) return true;
	else{
		for(byte i=2;i<=Math.sqrt(n);i++){
			if(n%i==0) return false;
			else if(i>Math.sqrt(n))return true;
		}
		return true;
	}
} 
}
