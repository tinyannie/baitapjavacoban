
public class Student implements Comparable<Student>{
	private String id;
	private String name;
	private int age;
	private String address;
	private double gpa;
	public Student(String id,String name,int age,String address,double gpa){
		this.id=id;
		this.name=name;
		this.age=age;
		this.address=address;
		this.gpa=gpa;
	}
	public String getId(){
		return id;
	}
	public String getName(){
		return name;
	}
	public int getAge(){
		return age;
	}
	public String getAddress(){
		return address;
	}
	public double getGpa(){
		return gpa;
	}
	public void setId(String newId){
		this.id=newId;
	}
	public void setName(String newName){
		this.name=newName;
	}
	public void setAge(int newAge){
		this.age=newAge;
	}
	public void setAddress(String newAddress){
		this.address=newAddress;
	}
	public void setGpa(double newGpa){
		this.gpa=newGpa;
	}
	public int compareTo(Student st) {
		// TODO Auto-generated method stub
		return (int) (this.gpa-st.gpa);
	}
}
