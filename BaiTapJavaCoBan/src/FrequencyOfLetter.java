import java.sql.Array;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
public class FrequencyOfLetter {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String inputString;
		System.out.print("Write something: ");
		inputString=sc.nextLine();
		System.out.println("Frequency of words: ");
		System.out.println(countFrequency(inputString));
	}
	private static Map countFrequency(String str){
		int i,j,count;
		Map<Character,Integer> freMap=new HashMap<Character,Integer>();
		char[] arr=str.toCharArray();
		for(i=0;i<arr.length;i++){
			count=0;
			if(arr[i]==' ') continue;
			for(j=i+1;j<arr.length;j++){
				if(arr[j]==arr[i]){
					count++;
					arr[j]=' ';
				}
			}
			freMap.put(arr[i], count+1);
			arr[i]=' ';
		}
		return freMap;
	}
}
